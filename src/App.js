import React, { Component } from 'react';
import { Stage, Layer, } from 'react-konva';
import Node from "./components/Node";
import Connection from "./components/Connection";
import { saveConceptMap, loadConceptMap } from "./functionMethods/saveConceptMap"
import {config}  from "./components/configs"

/* TODO list of things to do
** 
lasso selection
*/

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      nodes: [],
      lines: [],
      selectedNodeId: null,
      isSquareSelected: false,
      isEnteringText: false,
      inputText: "",
      data: {},
      drawLine: false,
      selectedLineId: null,
      isLineSelected: false,

    };


    this.stageDblClickHandler = this.stageDblClickHandler.bind(this);

    this.stageOnClick = this.stageOnClick.bind(this);

    this.nodeSelectionHandler = this.nodeSelectionHandler.bind(this);
    this.createLineHandler = this.createLineHandler.bind(this);
    this.stageOnMouseMove = this.stageOnMouseMove.bind(this);
    this.stageOnMouseUp = this.stageOnMouseUp.bind(this)
    this.nodeDragHandler = this.nodeDragHandler.bind(this);
    // this.handleKeyDown = this.handleKeyDown.bind(this);

    this.handleTextInputChange = this.handleTextInputChange.bind(this);
    // this.textInputSelected = this.textInputSelected.bind(this);

    this.drawLine = this.drawLine.bind(this) // REVIEW this has to be renewed
    // Save
    this.saveConceptMapDelegator = this.saveConceptMapDelegator.bind(this);
    // Clear
    this.clearCanvas = this.clearCanvas.bind(this);
    // load from memory
    this.loadConceptMap = this.loadConceptMap.bind(this);

    this.nodeWidthNotifier = this.nodeWidthNotifier.bind(this);

    this.lineClickHandler = this.lineClickHandler.bind(this);

    this.isEnteringTextCallback = this.isEnteringTextCallback.bind(this)
    this.onKeyPress = this.onKeyPress.bind(this)
  }

  createId(){
    return Math.random().toString(36).substr(2, 9);
  }

  getUniqueId(id, type){

    if(type == "node"){
      var nodes = [...this.state.nodes]
      var target = nodes.find(node => node.id === id);
      if (target){
        return this.getUniqueId(this.createId(), "node")
      }else{
        return id;
      }
      

    }if(type == "line"){
      var lines = [...this.state.lines]
      var target = lines.find(line => line.id === id);
      if (target){
        return this.getUniqueId(this.createId(), "line")
      }else{
        return id;
      }

    }

    return id;
  }


  createNode(x, y, fill, type) {
    var boxW = (type == 'Concept') ? config.ConceptNodeWidth : config.LinkNodeWidth;
    var boxH = (type == 'Concept') ? config.ConceptNodeHeight : config.LinkNodeHeight;
    var id = this.getUniqueId(this.createId(), "node");

    var stroke = (type == 'Concept') ? config.ConceptNodeStrokeColor : config.LinkNodeStrokeColor;
    var node = {
      key: id,
      id: id,
      x: x - boxW / 2,
      y: y - boxH / 2,
      width: boxW,
      height: boxH,
      fill: fill,
      draggable: true,
      active: false,
      stroke: stroke,
      name: type,
      connections: [],
      text: '???',
      isTextListening: false,

    };

    return node;
  }

  /**
   * if a different node is selected from the current selected one
   * it deactivates the previous one and activates the most recently selected.
   */
  nodeSelectionHandler(data) {

    this.deactivateLine();
    var nodes = [...this.state.nodes];


    nodes.forEach((node) => {
      if (node.id === data.id) {

        node.active = true;
        node.x = data.x;
        node.y = data.y;
        node.stroke = config.NodeSelectedColor;

      }
      else {
        node.active = false;
        node.isTextListening = false;
        node.stroke = (node.name == 'Concept') ? config.ConceptNodeStrokeColor : config.LinkNodeStrokeColor;
      }
    });

    this.setState({
      isSquareSelected: true,
      selectedNodeId: data.id,
      isEnteringText: false,
      nodes: nodes,
    });
  }

  createLine(x1, y1, x2, y2, node1, node2) {
    // var lineId = this.createId();
    var lineId = this.getUniqueId(this.createId(), 'line');
    var line = {
      key: lineId,
      id: lineId,
      x1: x1,
      y1: y1,
      x2: x2,
      y2: y2,
      nodeId1: node1,// the node from which the line is created (x1, y1)
      nodeId2: node2, // node to which is connected (x2, y2)
      stroke: config.LineColor,
    }

    return line
  }

  /**
   * Handling of movement of a node
   * When a node is dragged in the canvas this method constantly updates the 
   * x,y position of the Node and the lines connected to it if any are present.
   * 
   * This function is passed to the node as a prop, which gets an object containing
   * the node state.
   */
  nodeDragHandler(nodeData) {

    var lines = [...this.state.lines]
    var nodes = [...this.state.nodes];

    nodes.forEach((node) => {
      if (node.id === nodeData.id) {

        var conns = node.connections;
        node.x = nodeData.x;
        node.y = nodeData.y;

        conns.forEach((con) => {
          lines.forEach((line) => {
            if (con === line.id) {
              if (line.nodeId1 == node.id) {

                line.x1 = nodeData.x + nodeData.width / 2;
                line.y1 = nodeData.y + nodeData.height / 2;

              } else if (line.nodeId2 == node.id) {

                line.x2 = nodeData.x + nodeData.width / 2;
                line.y2 = nodeData.y + nodeData.height / 2;
              }
            }
          });
        });
      }
    });

    this.setState({
      lines: lines,
      nodes: nodes,
    })

  }

  // ======================== CONNECTING FUNCTIONS ===========================

  // returns true if nodes can connect 
  nodesCanConnect(targetNode) {
    // if a Link node has already a connection to a Connection node
    // It cannot reconnect.


    var selectedNode = this.state.data;

    var t_name = targetNode.name;
    var t_id = targetNode.id;
    var s_name = selectedNode.name;
    var s_id = selectedNode.id;

    var flag = true;
    if (s_id === t_id) {
      console.log('same node - not connecting');
      flag = false;
    }
    // if a Link - Concept connection will be made
    if ((t_name === 'Concept' && s_name == 'Link') || (t_name === 'Link' && s_name == 'Concept')) {

      var nodes = [...this.state.nodes];
      // get the target node
      var target = nodes.find(node => node.id === t_id).connections;
      // get the selected node
      var selected = nodes.find(node => node.id === s_id).connections;
      // check if the target and the selected node already share a connection
      target.forEach((conn) => {
        var found = selected.find(s_c => s_c === conn)
        if (found){
          console.log('Not able to connect to same Link-Concept Node');

          flag = false;
          return;

        }
        
      });
    }

    return flag;
  }

  // connect two nodes
  connectNodes(targetNode) {

    
    if (!this.nodesCanConnect(targetNode)) {
      return false;
    }

    var nodes = [...this.state.nodes];

    const lines = [...this.state.lines];
    // lastLine will connect the node is created from to the text node
    var lastLine = lines[lines.length - 1];


    lines.forEach((line) => {
      if (line.id === lastLine.id) {

        var selectedNodeId = this.state.selectedNodeId;

        var selectedNode = nodes.find(node => node.id === selectedNodeId);
        // x, y coordinate of the selected node center
        var X_1 = selectedNode.x + selectedNode.width / 2;
        var Y_1 = selectedNode.y + selectedNode.height / 2;

        // x,y coordinates of the target node center
        var X_2 = targetNode.x + targetNode.width / 2;
        var Y_2 = targetNode.y + targetNode.height / 2;

        // middle x,y of the two nodes to position a node and put the link node
        var midX;
        var midY;

        if (X_2 > X_1) {
          midX = X_1 + (X_2 - X_1) / 2;
        } else {
          midX = X_2 + (X_1 - X_2) / 2;
        }
        if (Y_2 > Y_1) {
          midY = Y_1 + (Y_2 - Y_1) / 2;
        } else {
          midY = Y_2 + (Y_1 - Y_2) / 2;
        }

        // Link node x,y coordinates
        var tx1 = midX;
        var tx2 = midY;

        var linkNode = this.createNode(tx1, tx2, config.LinkNodeColor, 'Link');

        // setting the second Connection id of the first line to the Link Node id
        line.nodeId2 = linkNode.id;

        nodes = [...nodes, linkNode];

        // line connecting node 1 to Link Node coordinates
        line.x2 = midX;
        line.y2 = midY;

        // second line connecting text to target node x1, y1, x2, y2 coordinates
        var sx1 = midX;
        var sy1 = midY;
        var sx2 = targetNode.x + targetNode.width / 2;
        var sy2 = targetNode.y + targetNode.height / 2;

        // creating the second line
        var secondLine = this.createLine(sx1, sy1, sx2, sy2, linkNode.id, targetNode.id)

        lines.push(secondLine);

        nodes.forEach((node) => {
          if (node.id === linkNode.id) {
            node.connections = [...node.connections, line.id, secondLine.id];
          }

          else if (node.id === targetNode.id) {
            node.connections = [...node.connections, secondLine.id]
          }
          else if (node.id === this.state.data.id) {
            node.connections = [...node.connections, line.id]
          }
        })

      }

    })
    this.setState({
      nodes: nodes,
      lines: lines,
    });

    return true;
  }

  // ======================================================================


  // Link connecting function
  connectToLink(targetNode) {


    if (!this.nodesCanConnect(targetNode)) {
      return false;
    }

    var nodes = [...this.state.nodes];

    const lines = [...this.state.lines];
    // lastLine will connect the node is created from to the text node
    var lastLine = lines[lines.length - 1]

    lines.forEach((line) => {
      if (line.id === lastLine.id) {

        lastLine.x2 = targetNode.x + targetNode.width / 2;
        lastLine.y2 = targetNode.y + targetNode.height / 2;
        lastLine.nodeId2 = targetNode.id;


        nodes.forEach((node) => {

          if (node.id === targetNode.id) {
            node.connections = [...node.connections, line.id]
          }
          else if (node.id === this.state.data.id) {
            node.connections = [...node.connections, line.id]
          }
        })
      }
    })

    this.setState({
      lines: lines,

    })
    return true;
  }

  // ==============================  STAGE  ============================================

  // On double click on the stage creates a new node
  stageDblClickHandler(e) {
    
    // to avoid stage dblclick listening and creating a Node on top of another node    
    if (this.state.isLineSelected || this.state.isSquareSelected) {
      return;
    }

    let pp = e.target.getStage().getPointerPosition();

    var newNode = this.createNode(pp.x, pp.y, config.ConceptNodeColor, 'Concept');
    this.setState(state => ({
      nodes: [...this.state.nodes, newNode],
    }));
  }

  // deactivate last selected node
  deactivateNode() {

    // deactivates last selected square
    if (this.state.isSquareSelected) {
      var nodes = [...this.state.nodes];
      let lastSelected = this.state.selectedNodeId;

      var node = nodes.find(node => node.id === lastSelected);
      node.active = false;
      node.stroke = (node.name == 'Concept') ? config.ConceptNodeStrokeColor : config.LinkNodeStrokeColor;
      node.isTextListening = false;
      if (this.state.inputText) {
        node.text = this.state.inputText;
      }

      this.setState({
        nodes: nodes,
        selectedNodeId: null,
        isSquareSelected: false
      });
    }

  }

  // on click of the stage
  // if a node is selected, it deactivates it.
  stageOnClick(e) {


    if (e.target.hasName('saveMap') || e.target.hasName('Stage')) {

      this.deactivateNode();
      this.deactivateLine();
    }

    if (!e.target.hasName('Concept')) {

      return;
    }

  }

  drawLine(e) {
    
    if (!this.state.drawLine) {
      return;

    }
    var pp = e.target.getStage().getPointerPosition();

    const allLines = [...this.state.lines];

    let lastLine = allLines[allLines.length - 1];

    lastLine.x2 = pp.x;
    lastLine.y2 = pp.y;

    allLines[allLines.length - 1] = lastLine;

    this.setState({
      lines: allLines,
    });

  }

  // HACK this has to be removed and a better solution for drawLine has to be done.
  // The mousemove on stage
  // is used to draw the line from a node
  stageOnMouseMove(e) {

    // TODO toggle lasso selection function

  }

  stageOnMouseUp(e) {
    let pp = e.target.getStage().getPointerPosition();

    if (!this.state.drawLine) {
      return;
    }


    var targetAttrs = e.target.getAttrs();

    var targetName = targetAttrs.name;
    var success = false;
    // if the mouse is above a stage 
    // it creates a new node

    var selectedType = this.state.data.name;

    // connections from node 
    if (selectedType === 'Concept') {
      // from node to empty stage creates a new node and connects them to the link node
      if (targetName === 'Stage' || e.target.getClassName() === 'Line') {

        var newNode = this.createNode(pp.x, pp.y, config.ConceptNodeColor, 'Concept');

        this.setState(state => ({
          nodes: [...this.state.nodes, newNode],
        }));
        success = this.connectNodes(newNode);
      }
      // connects to another node and creates a link node
      else if (targetName === 'Concept') {
        success = this.connectNodes(targetAttrs);
      }
      // connects to a link node
      else if (targetName === 'Link') {
        success = this.connectToLink(targetAttrs)

      }
    }
    else if (selectedType === 'Link') {
      // no connection from link to another link
      if (targetName === 'Link') {
        success = false;

      }
      else if (targetName === 'Concept') {
        // from Link to Concept node connection
    
        success = this.connectToLink(targetAttrs)
      } else if (targetName === 'Stage' || e.target.getClassName() === 'Line') {
        var newNode = this.createNode(pp.x, pp.y, config.ConceptNodeColor, 'Concept');
        this.setState(state => ({
          nodes: [...this.state.nodes, newNode],
        }));

        success = this.connectToLink(newNode);
      }
    }


    var lines = [...this.state.lines];
    // if not successful connection
    // then delete the created line
    if (!success) {
      lines.pop();
    }
    this.setState({
      drawLine: false,
      lines: lines,

    });
    if (e.target.getType() !== 'Shape') {

      e.target.batchDraw();
    }

  }

  // ======================== DELETE Methods =============================================
  // deletes the node and its connections
  deleteNode() {
    var selectedNodeId = this.state.selectedNodeId;
    var nodes = [...this.state.nodes];
    var lines = [...this.state.lines];

    var selectedNode = nodes.find(node => node.id === selectedNodeId);
    var selectedNodeConnections = selectedNode.connections;
    var nodeName = selectedNode.name

    if (nodeName === 'Link') {
      // Removing connection for all nodes connected to the selected node
      nodes.forEach((node) => {
        if (node.id !== selectedNode.id) {
          var nodeConn = node.connections

          selectedNodeConnections.forEach((connection) => {
            // find if connection is in this node
            var conIndex = nodeConn.indexOf(connection);
            if (conIndex > -1) {
              node.connections.splice(conIndex, 1);
            }
          });
        }
      });

      var nodeToRemoveIndex = nodes.map(function (node) { return node.id; }).indexOf(selectedNodeId);
      // remove object
      nodes.splice(nodeToRemoveIndex, 1);

      selectedNodeConnections.forEach((con) => {
        var lineToRemoveIndex = lines.map(function (line) { return line.id; }).indexOf(con);
        lines.splice(lineToRemoveIndex, 1);
      });

    } else if (nodeName === 'Concept') {

      selectedNodeConnections.forEach((connection) => {
        // find the line with this connection id
        var line = lines.find(line => line.id === connection);
        var selNodeId = line.nodeId1;
        var linkNodeId = line.nodeId2;
        if (selNodeId !== selectedNodeId) {
          var temp = selNodeId;
          selNodeId = linkNodeId;
          linkNodeId = temp;
        }

        nodes.forEach((node) => {
          // if the node is the found link node
          if (node.id === linkNodeId) {
            var linkConnections = node.connections;


            //if link has more than two connections
            if (linkConnections.length > 2) {
              // remove lineId from node
              var lineConIndex = linkConnections.indexOf(connection);
              node.connections.splice(lineConIndex, 1);
              // remove line form lines array
              var lineToRemoveIndex = lines.map(function (line) { return line.id; }).indexOf(connection);
              lines.splice(lineToRemoveIndex, 1);


            } // =========================================================

            else if (linkConnections.length == 2) {// if link has only two connections
              var LinkSecondConnectionId;
              // get the second connection id
              linkConnections.forEach((con) => {
                if (con !== connection) {
                  LinkSecondConnectionId = con;
                }
              });
              // retrieve the second connection line
              var secondLine = lines.find(line => line.id === LinkSecondConnectionId);
              var linkNode = secondLine.nodeId1;
              var secondConceptNodeId = secondLine.nodeId2;
              if (linkNode != linkNodeId) {
                var temp = linkNode;
                linkNode = secondConceptNodeId;
                secondConceptNodeId = temp;
              }

              nodes.forEach((node) => {
                if (node.id === secondConceptNodeId) {
                  var lineConIndex = node.connections.indexOf(LinkSecondConnectionId);
                  node.connections.splice(lineConIndex, 1);
                }
              });
              var lineToRemoveIndex = lines.map(function (line) { return line.id; }).indexOf(connection);
              lines.splice(lineToRemoveIndex, 1);

              var lineToRemoveIndex = lines.map(function (line) { return line.id; }).indexOf(LinkSecondConnectionId);
              lines.splice(lineToRemoveIndex, 1);

              var nodeToRemoveIndex = nodes.map(function (node) { return node.id; }).indexOf(linkNodeId);
              // remove object
              nodes.splice(nodeToRemoveIndex, 1);

            }
          }
        });
      });

      var nodeToRemoveIndex = nodes.map(function (node) { return node.id; }).indexOf(selectedNodeId);
      // remove object
      nodes.splice(nodeToRemoveIndex, 1);

    }

    this.setState({
      selectedNodeId: null,
      isSquareSelected: false,
      nodes: nodes,
      lines: lines,


    })
  }

  /**
   * Deletes the line connecting two nodes
   * If the line is connected to a link with a single other connection
   * in the form of (N - L - N), it deletes both lines connected to the link node
   * and handles the removing of the connection in the two nodes N.
   */
  deleteLine() {

    var lines = [...this.state.lines];
    var nodes = [...this.state.nodes];

    var selectedLineId = this.state.selectedLineId;
    // find the selected line
    var line = lines.find((line) => line.id === selectedLineId);

    // get the node Id connected to this line
    var node1Id = line.nodeId1;
    var node2Id = line.nodeId2;
    // find the two nodes
    var n1 = nodes.find((node) => node.id === node1Id);
    var n2 = nodes.find((node) => node.id === node2Id);


    // define which one is the Node and switch the nodes if necessary.
    if (n1.name == 'Concept') {
      var temp = n1;
      n1 = n2;
      n2 = temp;

      var temp2 = node1Id;
      node1Id = node2Id;
      node2Id = temp2;

    }
    // get the two nodes connections
    var n1c = n1.connections;
    var n2c = n2.connections;

    if (n1.name == 'Link') {

      if (n1c.length > 2) {

        // remove lineConnectionId from Link 
        var connToRemove1 = n1c.map(function (con) { return con }).indexOf(selectedLineId);
        n1c.splice(connToRemove1, 1);
        // remove lineConnectionId from Node
        var connToRemove2 = n2c.map(function (con) { return con }).indexOf(selectedLineId);
        n2c.splice(connToRemove2, 1);

        // remove line from lines
        var lineToRemoveIndex = lines.map(function (line) { return line.id; }).indexOf(selectedLineId);
        lines.splice(lineToRemoveIndex, 1);

      } else if (n1c.length === 2) { // if the link node is connected to only 2 Nodes it deletes the whole connection 

        // remove connection from first Node
        var connToRemove2 = n2c.map(function (con) { return con }).indexOf(selectedLineId);
        n2c.splice(connToRemove2, 1);

        // get SecondLineId
        var secondLineId = n1c.find((c) => c !== selectedLineId);

        // get second Line
        var secondLine = lines.find((line) => line.id === secondLineId);
        // get id of nodes connected to the second line
        var secNodeId1 = secondLine.nodeId1;
        var secNodeId2 = secondLine.nodeId2;
        // get node1 and node 2 connected to line
        var secNode1 = nodes.find((node) => node.id === secNodeId1);
        var secNode2 = nodes.find((node) => node.id === secNodeId2);

        if (secNodeId1 === node1Id) { // it is the link node
          // remove the link from teh secNode2

          var secN2c = secNode2.connections;
          var node2toRemove = secN2c.map(function (con) { return con }).indexOf(secondLineId);
          secN2c.splice(node2toRemove, 1);


        } else if (secNodeId2 === node1Id) {

          var secN1c = secNode1.connections;
          var node1toRemove = secN1c.map(function (con) { return con }).indexOf(secondLineId);
          secN1c.splice(node1toRemove, 1);
        }

        var lineToRemoveIndex = lines.map(function (line) { return line.id; }).indexOf(selectedLineId);
        lines.splice(lineToRemoveIndex, 1);

        var secLineToRemoveIndex = lines.map(function (line) { return line.id; }).indexOf(secondLineId);
        lines.splice(secLineToRemoveIndex, 1);

        var nodeToRemoveIndex = nodes.map(function (node) { return node.id; }).indexOf(node1Id);
        // remove object
        nodes.splice(nodeToRemoveIndex, 1);
      }

    }

    this.setState({
      selectedLineId: null,
      isLineSelected: false,
      lines: lines,
      nodes: nodes,
    })
  }

  deleteHandler() {

    var selectedNodeId = this.state.selectedNodeId;
    if (this.state.isLineSelected) {

      this.deleteLine()
    }
    // if a node is  selected and it enters text
    if (selectedNodeId && this.state.isEnteringText) {
      this.deleteTextHandler();
      return;
    }
    if (!selectedNodeId) {
      console.log('delete !selectedNodeId');

      return;
    }
    if (this.state.isSquareSelected) {
      this.deleteNode()
    }
  }


  nodeWidthNotifier(nodeData) {

    var nodes = [...this.state.nodes];
    var lines = [...this.state.lines];

    var connections = nodeData.connections;

    var id = nodeData.id;
    var w = nodeData.width;
    var node = nodes.find(node => node.id === id);
    node.width = w;

    connections.forEach((conn) => {
      var c = lines.find((line) => line.id === conn);

      if (c.nodeId1 === id) {
        c.x1 = node.x + node.width / 2;
        c.y1 = node.y + node.height / 2;
      } else if (c.nodeId2 === id) {
        c.x2 = node.x + node.width / 2;
        c.y2 = node.y + node.height / 2;
      }
    });

    this.setState({

      nodes: nodes,
    })

  }


  // =========================== TEXT & TEXT FIELD ======================================

  handleTextInputChange(char) {

    var nodes = [...this.state.nodes];
    var lastSelected = this.state.selectedNodeId;

    var node = nodes.find(node => node.id === lastSelected);

    var prev = node.text;
    if (prev === '???') {
      node.text = char

    } else {
      node.text = prev + char
    }

    this.setState({ nodes: nodes })
  }

  deleteTextHandler() {


    var nodes = [...this.state.nodes];
    var lastSelected = this.state.selectedNodeId;

    var node = nodes.find(node => node.id === lastSelected);

    var prev = node.text;

    node.text = prev.slice(0, -1);

    this.setState({ nodes: nodes })

  }

  isEnteringTextCallback(flag) {

    this.setState({ isEnteringText: flag })
  }


  // ====================== SAVING / LOADING CONCEPT MAP ===============
  saveConceptMapDelegator() {

    this.deactivateNode();
    saveConceptMap(this.state.nodes, this.state.lines);

  }


  // ======================= LOAD / DELETE / SAVE OPERATIONS ===========================

  loadConceptMap() {
    var conceptMap = loadConceptMap();

    var concepts = JSON.parse(conceptMap[0]);
    var lines = JSON.parse(conceptMap[1]);
    if (!concepts.length) {
      return;
    }
    this.clearCanvas();

    this.setState({
      nodes: concepts,
      lines: lines,
    });

  }


  clearCanvas() {
    this.setState({
      nodes: [],
      lines: [],
      selectedNodeId: null,
      isSquareSelected: false,
      isEnteringText: false,
      inputText: "",
      data: {},
      drawLine: false,
    });

  }

  // ========================= LINE HANDLING ========================================
  // Create new Line 
  // called on mouse down on green rectangle
  createLineHandler(nodeData, e) {

    var pp = e.target.getStage().getPointerPosition();

    var line = this.createLine(nodeData.x, nodeData.y, pp.x, pp.y, nodeData.id, -1);

    this.setState({
      data: nodeData,
      drawLine: true,
      lines: [...this.state.lines, line],

    });

  }

  deactivateLine() {

    if (!this.state.isLineSelected) {
      return
    }

    var lines = [...this.state.lines]
    var lineId = this.state.selectedLineId;

    var line = lines.find((line) => line.id === lineId);
    line.stroke = config.LineColor;


    this.setState({
      selectedLineId: null,
      isLineSelected: false,
      lines: lines,
    });
  }

  lineClickHandler(lineId) {

    this.deactivateNode();

    var lines = [...this.state.lines];
    if (this.state.isLineSelected) {
      var line = lines.find((line) => line.id === this.state.selectedLineId);
      line.stroke = config.LineColor;
    }

    var line = lines.find((line) => line.id === lineId);

    line.stroke = config.LineSelectedColor;

    this.setState({
      selectedLineId: lineId,
      isLineSelected: true,
      lines: lines,
    })

  }

  // ==============================================================================
  getLinePoints(line) {

    return [line.x1, line.y1, line.x2, line.y2]
  }


  // ==============================================================================

  onKeyPress(e) {
    var evtType = e.type;
    var Which = e.which;
    var keyCode = e.keyCode;


    if (evtType === 'keydown' && keyCode == 8) {
      // Delete key
      this.deleteHandler()


      return
    }
    if (evtType === 'keypress' && keyCode == 0) {

      if (this.state.isEnteringText) {
        if (Which == 13) {
          // TODO Implement return key in text input -> should Resize height of box

          this.handleTextInputChange("\n");
        } else {
          var c = String.fromCharCode(Which);

          this.handleTextInputChange(c)
        }
      }


      return
    }

  }

  // ============================     RENDER     =====================================
  render() {



    return (

      <div tabIndex={1} onKeyDown={this.onKeyPress} onKeyPress={this.onKeyPress}>
        <button onClick={this.saveConceptMapDelegator}  >Save</button>
        <button onClick={this.clearCanvas}>ClearCanvas</button>
        <button onClick={this.loadConceptMap}>Load</button>

        <Stage width={window.innerWidth} height={window.innerHeight}
          onDblClick={this.stageDblClickHandler}
          onMouseMove={this.drawLine}
          onMouseUp={this.stageOnMouseUp}
          onClick={this.stageOnClick}
          name={'Stage'}
        >
          <Layer
            name={'Layer'}
          >
            {this.state.lines.map((line) => (
              <Connection
                key={line.key}
                id={line.id}
                points={this.getLinePoints(line)}
                stroke={line.stroke}
                lineClickHandler={this.lineClickHandler}

              />
            ))}

            {this.state.nodes.map((node) => (
              <Node
                {...node}

                nodeSelectionHandler={this.nodeSelectionHandler}
                createLineHandler={this.createLineHandler}
                nodeDragHandler={this.nodeDragHandler}
                nodeWidthNotifier={this.nodeWidthNotifier}
                isEnteringTextCallback={this.isEnteringTextCallback}
              />
            ))}
          </Layer>
        </Stage>
      </div>

    );
  }
}

export default App;

export function saveConceptMap(conceptArray, linesArray){


    var conceptDataString = JSON.stringify(conceptArray);
    var linesDataString =  JSON.stringify(linesArray);



    if(localStorage.getItem('conceptArray', conceptDataString)){

      localStorage.removeItem('conceptArray');
      localStorage.removeItem('linesArray');
      localStorage.setItem('conceptArray', conceptDataString);
      localStorage.setItem('linesArray', linesDataString);

    }else{
      
      localStorage.setItem('conceptArray', conceptDataString);
      localStorage.setItem('linesArray', linesDataString);

    }

}

export function loadConceptMap(){
  var c = localStorage.getItem('conceptArray');
  var l = localStorage.getItem('linesArray');

  return [c,l];
}


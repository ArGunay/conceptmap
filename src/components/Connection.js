import React, { Component } from 'react'
import { Line } from 'react-konva';
import {config}  from "./configs"

export default class Connection extends Component {


    static getDerivedStateFromProps(props, state) {
        if (props.active !== state.active) {

            return {
                stroke: props.stroke,
            };
        }
        return null;
    }

    constructor(props) {
        super(props);
        this.lineClick = this.lineClick.bind(this)
        this.state = {
            stroke:this.props.stroke,
            id : this.props.id
        }
    }

    lineClick(){

        this.props.lineClickHandler(this.state.id)
    }

    render() {
        return (
            <Line
                points={this.props.points}
                stroke={this.props.stroke}
                strokeWidth={config.lineStrokeWidth}
                onClick={this.lineClick}

            />
        )
    }
}



export const config = { 
    // Concept Node Configurations
    ConceptNodeWidth : 100,
    ConceptNodeHeight : 80,
    ConceptNodeColor : 'grey', //'azure',
    ConceptNodeStrokeColor : 'black',

    // Link Node Configurations
    LinkNodeWidth : 60,
    LinkNodeHeight : 40,
    LinkNodeColor : 'white',
    LinkNodeStrokeColor : 'grey',

    // General Node Configurations
    NodeInsertTextStrokeColor: 'green',
    NodeSelectedColor: 'blue',
    nodeStrokeWidth: 3,
    nodeCornerRadius: 10,

    // Connection configurations
    LineSelectedColor:'blue',
    LineColor: 'black',
    lineStrokeWidth: 6,

};

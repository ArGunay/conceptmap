import React, { Component } from 'react';
import { Group, Rect, Text } from 'react-konva';
import { config } from './configs';

class Node extends Component {
    // =================== CONSTRUCTOR =======================
    constructor(props) {
        super(props);
        this.state = {

            active: this.props.active,
            connections: this.props.connections,
            stroke: this.props.stroke,
            name: this.props.name,
            draggable: this.props.draggable,
            id: this.props.id,
            x: this.props.x,
            y: this.props.y,
            width: this.props.width,
            height: this.props.height,
            fill: this.props.fill,
            text: this.props.text,
            minWidth: 80,
            fontSize: 19,
            isTextListening: this.props.isTextListening,
            intervalId: -1,
        }
        this.onDragMove = this.onDragMove.bind(this);
        this.nodeMouseUp = this.nodeMouseUp.bind(this);
        this.drawLineMouseDown = this.drawLineMouseDown.bind(this);
        this.textDbClick = this.textDbClick.bind(this)
        this.textModification = this.textModification.bind(this);
    }

    // =================== State Handlers =======================
    static getDerivedStateFromProps(props, state) {
        if (props.active !== state.active) {
            if (!props.active) {
                clearInterval(state.intervalId);
                props.isEnteringTextCallback(props.isTextListening);
            }
            return {
                text: props.text,
                width: props.width,
                height: props.height,
                stroke: props.stroke,
                active: props.active,
                connections: props.connections,
                isTextListening: props.isTextListening,
            };
        }
        return null;
    }

    componentDidUpdate(prevProps) {
        if (prevProps.text !== this.props.text) {
            this.textWidthHandler(this.props.text);
            this.setState({ text: this.props.text })
        }
    }
    // =================== METHODS =======================
    getTargetPositions(e) {
        var x = e.target.getAttr('x');
        var y = e.target.getAttr('y');
        return { 'x': x, 'y': y }
    }
    onDragMove(e) {
        var newPos = this.getTargetPositions(e);
        this.setState({
            x: newPos.x,
            y: newPos.y,
        });
        var nodeData = {
            x: newPos.x,
            y: newPos.y,
            id: this.state.id,
            name: this.state.name,
            width: this.state.width,
            height: this.state.height,
            connections: this.state.connections,
        }
        this.props.nodeDragHandler(nodeData)
        e.cancelBubble = true;
    }
    // this is only for the node selection
    nodeMouseUp() {
        var rect = {
            'x': this.state.x,
            'y': this.state.y,
            'id': this.state.id,
            'stroke': this.state.stroke,
        };
        this.props.nodeSelectionHandler(rect);
    }

    // this handles the data necessary for connecting two nodes.
    // called on mouse down
    drawLineMouseDown(e) {
        var NodeData = {
            x: this.state.x + (this.state.width / 2),
            y: this.state.y + (this.state.height / 2),
            id: this.state.id,
            name: this.state.name,
            connections: this.state.connections,
        }
        // returning the data of the node, and e is the
        // arrow creator box data
        this.props.createLineHandler(NodeData, e);
        e.cancelBubble = true;
    }

    /** 
     * On change of the text this method resizes the width of the Node width
    */
    textWidthHandler(nextText) {
        var nexTextW = this.textNode.measureSize(nextText).width;
        // var nexTextH = this.textNode.measureSize(nextText).height;
        var width = nexTextW + 40
        if (width < 100) {
            width = 100;
        }
        var NodeData = {
            id: this.state.id,
            width: width,
            connections: this.state.connections,
        }
        this.setState({ width: width });
        this.props.nodeWidthNotifier(NodeData);
    }

    textModification() {
        // console.log(this.state.text);
    }

    textDbClick() {
        this.setState({ isTextListening: !this.state.isTextListening });
        var flag = this.state.isTextListening;
        if (flag) {
            // TODO setInterval to simulate cursor (?) Interval id is use in clearInterval
            // var id = setInterval(this.textModification, 1000)
            var id = 1;
            this.setState({ intervalId: id, stroke: config.NodeInsertTextStrokeColor})
        } else {
            // clearInterval(this.state.intervalId);
            this.setState({ intervalId: -1, stroke: config.NodeSelectedColor })
        }
        this.props.isEnteringTextCallback(flag);
    }

    // =================== RENDER =======================
    render() {
        return (
            <Group>
                {this.state.active && (<Rect
                    x={this.state.x + (this.state.width / 2) - 15}
                    y={this.state.y - 22}
                    width={30}
                    height={15}
                    fill="black"
                    name={'arrowCreatorBox'}
                    onMouseDown={this.drawLineMouseDown}
                />)}
                <Rect
                    name={this.state.name}
                    draggable={this.state.draggable}
                    id={this.state.id}
                    stroke={this.state.stroke}
                    x={this.state.x}
                    y={this.state.y}
                    width={this.state.width}
                    height={this.state.height}
                    fill={this.state.fill}
                    cornerRadius={config.nodeCornerRadius}
                    onClick={this.nodeMouseUp}
                    onDragMove={this.onDragMove}
                    onDblClick={this.textDbClick}
                    strokeWidth={config.nodeStrokeWidth}
                />
                <Text
                    listening={this.state.isTextListening}
                    text={this.state.text}
                    fontSize={this.state.fontSize}
                    fill={'#000'}
                    align={'center'}
                    x={this.state.x}
                    y={this.state.y + (this.state.height / 2) - this.state.fontSize / 2}
                    width={this.state.width}
                    // height={this.state.height}
                    onDblClick={this.textDbClick}
                    ref={textNode => {
                        this.textNode = textNode;
                    }}
                />
            </Group>
        );
    }
}
export default Node;

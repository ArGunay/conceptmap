# ConceptMap React Plugin

## Requirements

You need to have `Node.js` installed in your machine and use `npm` to install the dependencies.

## How to install

- Clone this repository to your local machine 
- `npm install` - This will install the required dependencies
- `npm start` — This will staty a server on port `3000`.
- Open a browser (preferably Chrome) and go to `localhost:3000`


## How to use
- Double click on the canvas to create a Concept node.
- Select the Concept and drag a connection line from it.
- Double click on the concept to enter text
- Select concept and connections and use backspace key to delete nodes and connections.

## Functionalities
- Saving the ConceptMap in the local history of the browser.
- Loading a ConceptMap from the local hsitroy of the browser.




---

This react app is initialized using `nano-react-app` which uses `parcel` as its bundler.

---


